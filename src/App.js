import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import Dashboard from './Components/Dashboard.jsx';
import Archive from './Components/Archive.jsx';
import Device from './Components/Device.jsx';
import Compare from './Components/Compare.jsx';
import User from './Components/User.jsx';
import Station from './Components/Station.jsx';
import Alert from './Components/Alert.jsx';
import Dashboard_1 from './Components/Dashboard_1.jsx';
import Dashboard_2 from './Components/Dashboard_2.jsx';
import Notification from './Components/Notification.jsx';
import Dashboard_1_loader from './Components/Dashboard_1_loader.jsx';
import Dashboard_2_loader from './Components/Dashboard_2_loader.jsx';
import Archive_loader from './Components/Archive_loader.jsx';
import Device_loader from './Components/Device_loader.jsx';
import Compare_loader from './Components/Compare_loader.jsx';
import Notification_loader from './Components/Notification_loader.jsx';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route exact path="/" component={Dashboard}/>
            <Route exact path="/dashboard_1" component={Dashboard_1}/>
            <Route exact path="/dashboard_2" component={Dashboard_2}/>
            <Route exact path="/dashboard_1_loader" component={Dashboard_1_loader}/>
            <Route exact path="/dashboard_2_loader" component={Dashboard_2_loader}/>
            <Route exact path="/archive" component={Archive}/>
            <Route exact path="/archive_loader" component={Archive_loader}/>
            <Route exact path="/device" component={Device}/>
            <Route exact path="/device_loader" component={Device_loader}/>
            <Route exact path="/compare" component={Compare}/>
            <Route exact path="/compare_loader" component={Compare_loader}/>
            <Route exact path="/user" component={User}/>
            <Route exact path="/station" component={Station}/>
            <Route exact path="/alert" component={Alert}/>
            <Route exact path="/notification" component={Notification}/>
            <Route exact path="/notification_loader" component={Notification_loader}/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;