import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Switch, Modal, Drawer, Card } from 'antd';
import './device.less';
import Head from './Head.jsx';
import Side from './Side.jsx';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];
const confirm = Modal.confirm;

const config = {
	chart: {
		renderTo: 'container',
		type: 'pie',
		plotBackgroundColor: null,
		plotBorderWidth: null,
		plotShadow: false,
		height: 360,
		width: window.innerWidth <= 425 ? window.innerWidth - 110 : 295
	},
	title: {
		text: ''
	},
	tooltip: {
		pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	},
	 plotOptions: {
		pie: {
			allowPointSelect: true,
			cursor: 'pointer',
			dataLabels: {
					enabled: false
			},
			showInLegend: true
		}
	},
		series: [{
				name: 'Online',
				colorByPoint: true,
				data: [{
						name: '>90%',
						y: 61.41,
						sliced: true,
						selected: true
				}, {
						name: '90-60%',
						y: 11.84
				}, {
						name: '<60%',
						y: 10.85
				}, {
						name: '0%',
						y: 4.67
				}]
		}]
};

function showDelete() {
	confirm({
		title: 'Do you want to delete ?',
		content: '',
		onOk() {},
		onCancel() {},
	});
}

const action = (
	<Menu>
		<Menu.Item key="action-1">Edit</Menu.Item>
		<Menu.Item key="action-2" onClick={showDelete}>Delete</Menu.Item>
	</Menu>
);

const AddForm = Form.create()(
	class extends React.Component {

		render() {
			const { visible, onCancel, onCreate } = this.props;
			
			return (
				<div id="create_form">
					<Drawer
						title="New Calibration"
						width={1000}
						placement="right"
						visible={visible}
						onClose={onCancel}
						maskClosable={false}
						style={{
							height: 'calc(100% - 55px)',
							overflow: 'auto',
							paddingBottom: 53,
						}}
					>
						<Form layout="vertical" hideRequiredMark>
							<Row gutter={16}>
								<Col span={6} className="wid-100">
									<Form.Item label="Calibration Type">
										<Select showSearch placeholder="Please select calibration type">
											<Option value="no2">NO2</Option>
											<Option value="co2">CO2</Option>
											<Option value="co">CO</Option>
											<Option value="pm2.5">PM2.5</Option>
											<Option value="pm10">PM10</Option>
											<Option value="noise">Noise</Option>
										</Select>
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={16}>
								<Col span={4} className="wid-100">
									<Form.Item label="WEzero">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="AUXzero">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Sensitivity">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Offset">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Multiplier">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
							</Row>
						</Form>
						<div
							style={{
								position: 'absolute',
								bottom: 0,
								width: '100%',
								borderTop: '1px solid #e8e8e8',
								padding: '10px 16px',
								textAlign: 'right',
								left: 0,
								background: '#fff',
								borderRadius: '0 0 4px 4px',
							}}
						>
							<Button
								style={{
									marginRight: 8,
								}}
								onClick={onCancel}
							>
								Cancel
							</Button>
							<Button onClick={onCancel} type="primary">Submit</Button>
						</div>
					</Drawer>
				</div>
			);
		}
	}
);

const data = [{
	station: 'Station-1',
	city: 'City-1',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-2',
	city: 'City-2',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-3',
	city: 'City-3',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-4',
	city: 'City-4',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-5',
	city: 'City-5',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-6',
	city: 'City-6',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-7',
	city: 'City-7',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-8',
	city: 'City-8',
	active: '13:34, 28 Sep'
}];

const userFilter = [{
	title: 'Status',
	value: 'status',
	key: '0-0',
	children: [{
		title: 'Online',
		value: 'online',
		key: '0-0-0',
	}, {
		title: 'Offline',
		value: 'offline',
		key: '0-0-1',
	}],
}, {
	title: 'City',
	value: 'city',
	key: '0-1',
	children: [{
		title: 'City-1',
		value: 'city-1',
		key: '0-1-0',
	}, {
		title: 'City-2',
		value: 'city-2',
		key: '0-1-1',
	}, {
		title: 'City-3',
		value: 'city-3',
		key: '0-1-2',
	}, {
		title: 'City-4',
		value: 'city-4',
		key: '0-1-2',
	}, {
		title: 'City-5',
		value: 'city-5',
		key: '0-1-2',
	}, {
		title: 'City-6',
		value: 'city-6',
		key: '0-1-2',
	}, {
		title: 'City-7',
		value: 'city-7',
		key: '0-1-2',
	}, {
		title: 'City-8',
		value: 'city-8',
		key: '0-1-2',
	}],
}];

const options1 = [
	{ label: 'GPRS', value: 'gprs' },
	{ label: 'Wifi', value: 'wifi' },
	{ label: 'Ethernet', value: 'ethernet' }
];

const options2 = [
	{ label: 'Archive Station', value: 'archive' }
];

class Device extends React.Component {

	state = {
		value: [],
		tableVisible: true,
		drawAddVisible: false,
	};

	onChangeFilter = (value) => {
		this.setState({ value });
	}

	showConfigDevice (){
		this.setState({
			tableVisible: false,
		});
	};

	showTable () {
		this.setState({
			tableVisible: true,
		})
	};

	showAddDrawer () {
		this.setState({ drawAddVisible: true });
	}

	handleCancel () {
		this.setState({ drawAddVisible: false });
	}

	render () {
		const { startValue, endValue, endOpen } = this.state;
		const userProps = {
			treeData: userFilter,
			value: this.state.value,
			onChange: this.onChangeFilter,
			treeCheckable: true,
			showCheckedStrategy: SHOW_PARENT,
			searchPlaceholder: 'Please select filter',
		};

		const columns = [{
			title: 'Station Name',
			width: 150,
			key: 'station',
			dataIndex: 'station',
			sorter: (a, b) => a.station.length - b.station.length,
		}, {
			title: 'City',
			width: 120,
			dataIndex: 'city',
			key: 'city',
			sorter: (a, b) => a.city.length - b.city.length,
		}, {
			title: 'Active',
			width: 80,
			dataIndex: 'active',
			key: 'active',
			sorter: (a, b) => a.active.length - b.active.length,
			render: () => (
				<Tooltip title="Last data received">
					<span className="dot"></span>
					<span className="date-time">13:34, 28 Sep</span>
				</Tooltip>
			),
		}, {
			title: 'Sync',
			width: 50,
			align: 'center',
			key: 'sync',
			render: () => (
				<Tooltip title="In Sync">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px" className="sync-img"><path fill="none" stroke="green" stroke-width="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31-132.6 78-7.8-3.8-16.4-5.7-25.5-5.7-32.8 0-59.8 24.4-64 56C47 152.3 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"></path><path fill="green" d="M374.7 144.5L353.2 123 231 245.2l-38.5-38.5-21 21 50.6 51 .8-.2 9.2 9.3"></path></svg>
				</Tooltip>
			),
		}, {
			title: 'Connectivity',
			width: 70,
			align: 'center',
			key: 'connect',
			render: () => (
				<Tooltip title="Connected">
					<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="170 0 170 500" height="20" width="20"><path fill="green" d="M64 402H9c-3 0-5 1-6 3-2 1-3 3-3 6v55c0 3 1 5 3 7l6 2h55l7-2 2-7v-55l-2-6c-2-2-4-3-7-3zM283 292h-55l-6 3-3 6v165c0 3 1 5 3 7l6 2h55l7-2 2-7V301l-2-6c-2-2-4-3-7-3zM174 365h-55c-3 0-5 1-7 3s-2 4-2 7v91c0 3 0 5 2 7l7 2h55l6-2c2-2 3-4 3-7v-91c0-3-1-5-3-7l-6-3zM393 183h-55l-7 2-2 7v274l2 7 7 2h55l6-2c2-2 3-4 3-7V192c0-3-1-5-3-7l-6-2zM509 39c-2-2-4-2-7-2h-54c-3 0-5 0-7 2s-2 4-2 7v420c0 3 0 5 2 7l7 2h54l7-2c2-2 3-4 3-7V46c0-3-1-5-3-7z"></path></svg>
				</Tooltip>
			),
		}, {
			title: 'Health',
			width: 50,
			align: 'center',
			key: 'health',
			render: () => (
				<Tooltip title="Ok">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448.8 448.8" width="16"><path fill="green" d="M142.8 324l-107-107.2L0 252.4l142.8 143 306-306-35.7-36"></path></svg>
				</Tooltip>
			),
		}, {
			title: 'Analytics',
			width: 60,
			align: 'center',
			key: 'analytics',
			render: () => (
				<Tooltip title="View Analytics">
					<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 486.742 486.742" className="analytics-image"><path fill="#ddd" d="M33 362.37v78.9c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-138.8l-44.3 44.3c-9.4 9.3-21.4 14.7-34.3 15.6zM142 301.47v139.8c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-82.3c-13.9-.3-26.9-5.8-36.7-15.6l-41.9-41.9zM251 350.27v91c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-167.9l-69.9 69.9c-2.7 2.7-5.6 5-8.7 7zM432.7 170.17l-72.7 72.7v198.4c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2l-1.3-1.3z"></path><path fill="#ddd" d="M482.6 41.37c-2.9-3.1-7.3-4.7-12.9-4.7h-1.6c-28.4 1.3-56.7 2.7-85.1 4-3.8.2-9 .4-13.1 4.5-1.3 1.3-2.3 2.8-3.1 4.6-4.2 9.1 1.7 15 4.5 17.8l7.1 7.2c4.9 5 9.9 10 14.9 14.9l-171.6 171.7-77.1-77.1c-4.6-4.6-10.8-7.2-17.4-7.2-6.6 0-12.7 2.6-17.3 7.2L7.2 286.87c-9.6 9.6-9.6 25.1 0 34.7l4.6 4.6c4.6 4.6 10.8 7.2 17.4 7.2s12.7-2.6 17.3-7.2l80.7-80.7 77.1 77.1c4.6 4.6 10.8 7.2 17.4 7.2 6.6 0 12.7-2.6 17.4-7.2l193.6-193.6 21.9 21.8c2.6 2.6 6.2 6.2 11.7 6.2 2.3 0 4.6-.6 7-1.9 1.6-.9 3-1.9 4.2-3.1 4.3-4.3 5.1-9.8 5.3-14.1.8-18.4 1.7-36.8 2.6-55.3l1.3-27.7c.3-5.8-1-10.3-4.1-13.5z"></path></svg>
				</Tooltip>
			),
		}, {
			title: 'Configure',
			width: 60,
			align: 'center',
			key: 'configure',
			render: () => (
				<Tooltip title="Configure Device">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 451.8 451.8" width="22" height="20" className="configure-image" onClick={() => this.showConfigDevice()}><path fill="none" stroke="#DDD" stroke-width="56.5" d="M127.6 30.6L323 224 129.5 423" stroke-linecap="round" stroke-linejoin="round"></path></svg>
				</Tooltip>
			),
		}];

		return (
			<div id="device" className="mar-top-70">
				<Side active_link="device" />
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						{(() => {
							if (this.state.tableVisible) {
								return <div>
									<div className="head">Device Details</div>
									<Row type="flex" justify="space-around" className="device-details">
										<Col span={7} className="width-100">
											<div className="pie-text">Total Number of Devices -
												<span className="online-device"> 66</span>
												<span> </span>
												(<span className="success">54 </span>/<span className="danger"> 12</span>)
											</div>	
											<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
										</Col>

										<Col span={8} className="activity-details">
											<Card title="Recent Activities" className="back-grey">
												<div className="activity-container">
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-1</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-2</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-3</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-4</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-5</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-6</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-7</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-8</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-9</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-10</Col>
														<Col span={4} className="success">Online</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
													<Row type="flex" justify="space-between" className="activities">
														<Col span={6}>Device-11</Col>
														<Col span={4} className="danger">Offline</Col>
														<Col span={8}>11:50, 31 Aug</Col>
													</Row>
												</div>
											</Card>
										</Col>
									</Row>
									<div className="table-filter"><TreeSelect treeDefaultExpandAll {...userProps} className="filter-icon" /></div>
									<div className="table-search">
										<Input placeholder="Search User" prefix={<Icon type="search" />} />
									</div>
									<Row>
										<div className="show-container">
											Showing 
											<span className="show-txt">8</span>
											out of
											<span className="show-txt">20</span>
										</div>
									</Row>
									<Row>
										<Table columns={columns} dataSource={data} scroll={{ y: 540 }} />
									</Row>
								</div>
							}
							else {
								return <div className="configure-device">
									<div className="head top-60">
										<span className="dot1"></span>
										<span>Device-1</span>
									</div>
									<span className="back-btn">
										<Button icon="arrow-left" onClick={() => this.showTable()}>Back</Button>
									</span>
									<Tabs type="card">
										<TabPane tab="Details" key="details">
											<Form layout="vertical" hideRequiredMark>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="Device Name">
															<Input placeholder="Please enter device name" />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="Device Latitude">
															<Input placeholder="Please enter device latitude" />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="Device Lontitude">
															<Input placeholder="Please enter device longitude" />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="Location Full Address">
															<Input.TextArea rows={4} placeholder="Please enter location full address" />
														</Form.Item>
													</Col>
												</Row>
											</Form>
											<div className="butn-contain">
												<Button type="primary">Save</Button>
											</div>
										</TabPane>

										<TabPane tab="Configuration" key="config">
											<Form layout="vertical" hideRequiredMark>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="Modem Type">
															<Select showSearch placeholder="Please select operatoe">
																<Option value="gprs">GPRS</Option>
																<Option value="wifi">Wifi</Option>
																<Option value="ethernet">Ethernet</Option>
															</Select>
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="Data Transmission Interval">
															<Input placeholder="Please enter data transmission interval" />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="Data Sampling Interval">
															<Input placeholder="Please enter data sampling interval" />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="WiFi SSID">
															<Input placeholder="Please enter wifi ssid" />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="WiFi Password">
															<Input placeholder="Please enter wifi password" />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="First Server IP">
															<Input placeholder="Please enter first server ip" />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="First Server Port">
															<Input placeholder="Please enter first server port" />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="Secondary Server IP">
															<Input placeholder="Please enter secondary server ip" />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="Secondary Server Port">
															<Input placeholder="Please enter secondary server port" />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="Device IP">
															<Input placeholder="Please enter device ip" />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="Subnet Mask">
															<Input placeholder="Please enter subnet mask" />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="Default Gateway">
															<Input placeholder="Please enter default gateway" />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="DNS Server">
															<Input placeholder="Please enter dns server" />
														</Form.Item>
													</Col>
												</Row>
											</Form>
											<div className="butn-contain">
												<Button type="primary">Save</Button>
											</div>
										</TabPane>

										<TabPane tab="Calibration" key="calibration">
											<div className="btn-contain">
												<Button className="add-btn" type="primary" icon="plus" onClick={() =>this.showAddDrawer()}>Add New</Button>
											</div>
											<Form layout="vertical" className="calibration-form" hideRequiredMark>
												<Row gutter={16} className="calib">
													<Col span={5}>
														<Form.Item label="Calibration Type">
															<Select showSearch defaultValue="no2" placeholder="Please select calibration type">
																<Option value="no2">NO2</Option>
																<Option value="co2">CO2</Option>
																<Option value="co">CO</Option>
																<Option value="pm2.5">PM2.5</Option>
																<Option value="pm10">PM10</Option>
																<Option value="noise">Noise</Option>
															</Select>
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="WEzero">
															<Input placeholder="Please enter value" defaultValue="290" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="AUXzero">
															<Input placeholder="Please enter value" defaultValue="290" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Sensitivity">
															<Input placeholder="Please enter value" defaultValue="0.267" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Offset">
															<Input placeholder="Please enter value" defaultValue="0" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Multiplier">
															<Input placeholder="Please enter value" defaultValue="1" />
														</Form.Item>
													</Col>
													<Col span={2} className=" right">
														<Form.Item label=" ">
															<Dropdown overlay={action} trigger={['click']} placement="bottomLeft">
																<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 512" className="action-img"><path d="M204 102c28 0 51-23 51-51S232 0 204 0s-51 23-51 51 23 51 51 51zm0 51c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51zm0 153c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51z"/></svg>
															</Dropdown>
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={16} className="calib">
													<Col span={5}>
														<Form.Item label="Calibration Type">
															<Select showSearch defaultValue="co2" placeholder="Please select calibration type">
																<Option value="no2">NO2</Option>
																<Option value="co2">CO2</Option>
																<Option value="co">CO</Option>
																<Option value="pm2.5">PM2.5</Option>
																<Option value="pm10">PM10</Option>
																<Option value="noise">Noise</Option>
															</Select>
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Offset">
															<Input placeholder="Please enter value" defaultValue="0" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Multiplier">
															<Input placeholder="Please enter value" defaultValue="1" />
														</Form.Item>
													</Col>
													<Col span={2} className=" right">
														<Form.Item label=" ">
															<Dropdown overlay={action} trigger={['click']} placement="bottomLeft">
																<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 512" className="action-img"><path d="M204 102c28 0 51-23 51-51S232 0 204 0s-51 23-51 51 23 51 51 51zm0 51c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51zm0 153c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51z"/></svg>
															</Dropdown>
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={16} className="calib">
													<Col span={5}>
														<Form.Item label="Calibration Type">
															<Select showSearch defaultValue="o2" placeholder="Please select calibration type">
																<Option value="no2">NO2</Option>
																<Option value="co2">CO2</Option>
																<Option value="o2">O2</Option>
																<Option value="co">CO</Option>
																<Option value="pm2.5">PM2.5</Option>
																<Option value="pm10">PM10</Option>
																<Option value="noise">Noise</Option>
															</Select>
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Offset">
															<Input placeholder="Please enter value" defaultValue="0" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Multiplier">
															<Input placeholder="Please enter value" defaultValue="1" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="O2 V1">
															<Input placeholder="Please enter value" defaultValue="1.15" />
														</Form.Item>
													</Col>
													<Col span={2} className="right">
														<Form.Item label=" ">
															<Dropdown overlay={action} trigger={['click']} placement="bottomLeft">
																<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 512" className="action-img"><path d="M204 102c28 0 51-23 51-51S232 0 204 0s-51 23-51 51 23 51 51 51zm0 51c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51zm0 153c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51z"/></svg>
															</Dropdown>
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={16} className="calib">
													<Col span={5}>
														<Form.Item label="Calibration Type">
															<Select showSearch defaultValue="co" placeholder="Please select calibration type">
																<Option value="no2">NO2</Option>
																<Option value="co2">CO2</Option>
																<Option value="co">CO</Option>
																<Option value="pm2.5">PM2.5</Option>
																<Option value="pm10">PM10</Option>
																<Option value="noise">Noise</Option>
															</Select>
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="WEzero">
															<Input placeholder="Please enter value" defaultValue="315" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="AUXzero">
															<Input placeholder="Please enter value" defaultValue="283" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Sensitivity">
															<Input placeholder="Please enter value" defaultValue="0.262" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Offset">
															<Input placeholder="Please enter value" defaultValue="0" />
														</Form.Item>
													</Col>
													<Col span={3}>
														<Form.Item label="Multiplier">
															<Input placeholder="Please enter value" defaultValue="1" />
														</Form.Item>
													</Col>
													<Col span={2} className=" right">
														<Form.Item label=" ">
															<Dropdown overlay={action} trigger={['click']} placement="bottomLeft">
																<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 512" className="action-img"><path d="M204 102c28 0 51-23 51-51S232 0 204 0s-51 23-51 51 23 51 51 51zm0 51c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51zm0 153c-28 0-51 23-51 51s23 51 51 51 51-23 51-51-23-51-51-51z"/></svg>
															</Dropdown>
														</Form.Item>
													</Col>
												</Row>
											</Form>
											<AddForm 
												visible={this.state.drawAddVisible}
												onCancel={() =>this.handleCancel()}
												onCreate={() =>this.handleCancel()}
											/>
										</TabPane>
									</Tabs>
								</div>
							}
						})()}
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Device;