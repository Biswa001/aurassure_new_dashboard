import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Skeleton, Spin, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Switch, Modal, Drawer } from 'antd';
import './device.less';
import Head from './Head.jsx';
import Side from './Side.jsx';

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} />;

const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];
const confirm = Modal.confirm;

function showDelete() {
	confirm({
		title: 'Do you want to delete ?',
		content: '',
		onOk() {},
		onCancel() {},
	});
}

const action = (
	<Menu>
		<Menu.Item key="action-1">Edit</Menu.Item>
		<Menu.Item key="action-2" onClick={showDelete}>Delete</Menu.Item>
	</Menu>
);

const AddForm = Form.create()(
	class extends React.Component {

		render() {
			const { visible, onCancel, onCreate } = this.props;
			
			return (
				<div id="create_form">
					<Drawer
						title="New Calibration"
						width={1000}
						placement="right"
						visible={visible}
						onClose={onCancel}
						maskClosable={false}
						style={{
							height: 'calc(100% - 55px)',
							overflow: 'auto',
							paddingBottom: 53,
						}}
					>
						<Form layout="vertical" hideRequiredMark>
							<Row gutter={16}>
								<Col span={6} className="wid-100">
									<Form.Item label="Calibration Type">
										<Select showSearch placeholder="Please select calibration type">
											<Option value="no2">NO2</Option>
											<Option value="co2">CO2</Option>
											<Option value="co">CO</Option>
											<Option value="pm2.5">PM2.5</Option>
											<Option value="pm10">PM10</Option>
											<Option value="noise">Noise</Option>
										</Select>
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={16}>
								<Col span={4} className="wid-100">
									<Form.Item label="WEzero">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="AUXzero">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Sensitivity">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Offset">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
								<Col span={4} className="wid-100">
									<Form.Item label="Multiplier">
										<Input placeholder="Please enter value" />
									</Form.Item>
								</Col>
							</Row>
						</Form>
						<div
							style={{
								position: 'absolute',
								bottom: 0,
								width: '100%',
								borderTop: '1px solid #e8e8e8',
								padding: '10px 16px',
								textAlign: 'right',
								left: 0,
								background: '#fff',
								borderRadius: '0 0 4px 4px',
							}}
						>
							<Button
								style={{
									marginRight: 8,
								}}
								onClick={onCancel}
							>
								Cancel
							</Button>
							<Button onClick={onCancel} type="primary">Submit</Button>
						</div>
					</Drawer>
				</div>
			);
		}
	}
);

const data = [{
	station: 'Station-1',
	city: 'City-1',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-2',
	city: 'City-2',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-3',
	city: 'City-3',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-4',
	city: 'City-4',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-5',
	city: 'City-5',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-6',
	city: 'City-6',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-7',
	city: 'City-7',
	active: '13:34, 28 Sep'
}, {
	station: 'Station-8',
	city: 'City-8',
	active: '13:34, 28 Sep'
}];

const userFilter = [{
	title: 'Status',
	value: 'status',
	key: '0-0',
	children: [{
		title: 'Online',
		value: 'online',
		key: '0-0-0',
	}, {
		title: 'Offline',
		value: 'offline',
		key: '0-0-1',
	}],
}, {
	title: 'City',
	value: 'city',
	key: '0-1',
	children: [{
		title: 'City-1',
		value: 'city-1',
		key: '0-1-0',
	}, {
		title: 'City-2',
		value: 'city-2',
		key: '0-1-1',
	}, {
		title: 'City-3',
		value: 'city-3',
		key: '0-1-2',
	}, {
		title: 'City-4',
		value: 'city-4',
		key: '0-1-2',
	}, {
		title: 'City-5',
		value: 'city-5',
		key: '0-1-2',
	}, {
		title: 'City-6',
		value: 'city-6',
		key: '0-1-2',
	}, {
		title: 'City-7',
		value: 'city-7',
		key: '0-1-2',
	}, {
		title: 'City-8',
		value: 'city-8',
		key: '0-1-2',
	}],
}];

const options1 = [
	{ label: 'GPRS', value: 'gprs' },
	{ label: 'Wifi', value: 'wifi' },
	{ label: 'Ethernet', value: 'ethernet' }
];

const options2 = [
	{ label: 'Archive Station', value: 'archive' }
];

class Device_loader extends React.Component {

	state = {
		value: [],
		tableVisible: true,
		drawAddVisible: false,
	};

	handleCancel () {
		this.setState({ drawAddVisible: false });
	}

	render () {
		const { startValue, endValue, endOpen } = this.state;
		const userProps = {
			treeData: userFilter,
			value: this.state.value,
			onChange: this.onChangeFilter,
			treeCheckable: true,
			showCheckedStrategy: SHOW_PARENT,
			searchPlaceholder: 'Please select filter',
		};

		return (
			<div id="device" className="mar-top-70 device-loader">
				<Side active_link="device" />
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						{(() => {
							if (this.state.tableVisible) {
								return <div>
									<div className="head"><Skeleton className="hed-load" active /></div>
									<div className="table-filter"><Skeleton className="select-load" active /></div>
									<div className="table-search">
										<Skeleton className="select-load" active />
									</div>
									<Row>
										<Skeleton className="hed-load" active />
									</Row>
									<Row className="min-height">
										<Spin indicator={antIcon} />
									</Row>
								</div>
							}
							else {
								return <div className="configure-device">
									<div className="head top-60">
										<Skeleton className="hed-load" active />
									</div>
									<span className="back-btn">
									</span>
									<Tabs type="card">
										<TabPane tab="Details" key="details">
											<Form layout="vertical" hideRequiredMark>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
											</Form>
										</TabPane>

										<TabPane tab="Configuration" key="config">
											<Form layout="vertical" hideRequiredMark>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={50}>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={12} className="wid-100">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
											</Form>
										</TabPane>

										<TabPane tab="Calibration" key="calibration">
											<Form layout="vertical" className="calibration-form" hideRequiredMark>
												<Row gutter={16}>
													<Col span={5}>
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={3} className="">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={3} className="">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={3} className="">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={3} className="">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={3} className="">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
												<Row gutter={16}>
													<Col span={5} className="">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={3} className="">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
													<Col span={3} className="">
														<Form.Item label="">
															<Skeleton className="list-load" active />
														</Form.Item>
													</Col>
												</Row>
											</Form>
											<AddForm 
												visible={this.state.drawAddVisible}
												onCancel={() =>this.handleCancel()}
												onCreate={() =>this.handleCancel()}
											/>
										</TabPane>
									</Tabs>
								</div>
							}
						})()}
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Device_loader;