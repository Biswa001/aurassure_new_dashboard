import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Drawer, Radio, DatePicker, Checkbox, Form, Input, Table, TreeSelect } from 'antd';
import './archive.less';
import Head from './Head.jsx';
import Side from './Side.jsx';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';

const TabPane = Tabs.TabPane;
const { Content } = Layout;
const { Option } = Select;
const RadioGroup = Radio.Group;
const CheckboxGroup = Checkbox.Group;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];
const SHOW_PARENT = TreeSelect.SHOW_PARENT;

const graph1 = {
	chart: {
				type: 'line',
				height: 400
		},
		title: {
				text: ''
		},
		xAxis: {
				categories: ['DAY-1', 'DAY-2', 'DAY-3', 'DAY-4', 'DAY-5']
		},
		yAxis: {
				title: {
						text: ''
				},
				labels: {
						enabled: false
				}
		},
		plotOptions: {
				line: {
						dataLabels: {
								enabled: true
						},
						enableMouseTracking: true
				}
		},
		series: [{
			showInLegend: false, 
				name: '',
				data: [7.0, 6.9, 9.5, 7.5, 1.4]
		}]
};

const graph2 = {
	chart: {
				type: 'line',
				height: 400
		},
		title: {
				text: ''
		},
		xAxis: {
				categories: ['DAY-1', 'DAY-2', 'DAY-3', 'DAY-4', 'DAY-5']
		},
		yAxis: {
				title: {
						text: ''
				},
				labels: {
						enabled: false
				}
		},
		plotOptions: {
				line: {
						dataLabels: {
								enabled: true
						},
						enableMouseTracking: true
				}
		},
		series: [{
				name: 'Max',
				data: [7.0, 6.9, 9.5, 11.5, 7.4]
		}, {
				name: 'Min',
				data: [3.9, 4.2, 5.7, 8.5, 4.9]
		}]
};

const city = [];
for (let i = 1; i < 12; i++) {
	city.push(<Option key={'City - ' + i}>{'City - ' + i}</Option>);
}

const device = [];
for (let i = 1; i < 12; i++) {
	device.push(<Option key={'Device - ' + i}>{'Device - ' + i}</Option>);
}

function handleChange(value) {}

function onChange(value, dateString) {
	console.log('Selected Time: ', value);
	console.log('Formatted Selected Time: ', dateString);
}

function onOk(value) {
	console.log('onOk: ', value);
}

const data = [
	{
		parameter: 'Temperature(°C)',
		avg: 29.24,
		min: 25.40,
		minat: '03:37, 27 Sep',
		max: 36.20,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'Humidity(%)',
		avg: 92.17,
		min: 62.40,
		minat: '03:37, 27 Sep',
		max: 99.20,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'SO2(ppb)',
		avg: 33.51,
		min: 6.82,
		minat: '03:37, 27 Sep',
		max: 75.31,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'NO2(ppb)',
		avg: 11.03,
		min: 2.37,
		minat: '03:37, 27 Sep',
		max: 19.02,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'NO(ppb)',
		avg: 25.99,
		min: 4.30,
		minat: '03:37, 27 Sep',
		max: 48.65,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'CO(ppm)',
		avg: 0.3937,
		min: 0.2840,
		minat: '03:37, 27 Sep',
		max: 0.6961,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'O2(% vol.)',
		avg: 20.90,
		min: 20.23,
		minat: '03:37, 27 Sep',
		max: 26.14,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'Noise(dB (A))',
		avg: 33.08,
		min: 28.17,
		minat: '03:37, 27 Sep',
		max: 74.11,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'PM1(μg / m3)',
		avg: 49.44,
		min: 32.00,
		minat: '03:37, 27 Sep',
		max: 209.00,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'PM2.5(μg / m3)',
		avg: 67.81,
		min: 46.00,
		minat: '03:37, 27 Sep',
		max: 258.00,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'PM10(μg / m3)',
		avg: 83.06,
		min: 58.00,
		minat: '03:37, 27 Sep',
		max: 292.00,
		maxat: '11:37, 27 Sep'
	}];

const ShowData = Form.create()(
	class extends React.Component {

		constructor() {
			super()
			this.handleCheckBox = this.handleCheckBox.bind(this)
			this.state = {
				gridVisible: true
			}
		}

		handleCheckBox(e) {
			this.setState({
				gridVisible: !this.state.gridVisible
			})
		}

		render() {
			const { visible, onCancel, onCreate, form } = this.props;
			const { getFieldDecorator } = form;

			const columns = [{
				title: 'Parameter',
				width: 120,
				key: 'parameter',
				dataIndex: 'parameter'
			}, {
				title: 'Avg',
				width: 80,
				dataIndex: 'avg',
				key: 'avg'
			}, {
				title: 'Min',
				width: 80,
				dataIndex: 'min',
				key: 'min'
			}, {
				title: 'Min at.',
				width: 100,
				dataIndex: 'minat',
				key: 'minat'
			}, {
				title: 'Max',
				width: 80,
				dataIndex: 'max',
				key: 'max'
			}, {
				title: 'Max at.',
				width: 100,
				dataIndex: 'maxat',
				key: 'maxat'
			}];
			
			return (
				<div id="archive_report">
					<Drawer
						title="Archive Report"
						width={1000}
						placement="right"
						visible={visible}
						onClose={onCancel}
						maskClosable={true}
						style={{
							height: 'calc(100% - 55px)',
							overflow: 'auto',
							paddingBottom: 53,
						}}
					>
						<Form layout="vertical" hideRequiredMark className="archive-report">
							<div className="view-online">
								<span>Grid</span>
								<div className='wrapper'>
									<input type='checkbox' id='checkable1' onChange={this.handleCheckBox} checked={this.state.checked} />
									<label for='checkable1'></label>
								</div>
								<span>Graph</span>
							</div>
							<Tabs type="card" className="draw-tab">
								<TabPane tab="Device-1" key="device-1">
									{(() => {
										if (this.state.gridVisible) {
											return <div>
												<Table columns={columns} dataSource={data} scroll={{ y: 540 }} />
											</div>
										} else {
											return <div>
												<Row>
													<ReactHighcharts config={graph1} ref="graph"></ReactHighcharts>
												</Row>
												<Row>
													<ReactHighcharts config={graph2} ref="graph"></ReactHighcharts>
												</Row>
											</div>;
										}
									})()}
								</TabPane>
								<TabPane tab="Device-2" key="device-2">
									{(() => {
										if (this.state.gridVisible) {
											return <div>
												<Table columns={columns} dataSource={data} scroll={{ y: 540 }} />
											</div>
										} else {
											return <div>
												<Row>
													<ReactHighcharts config={graph2} ref="graph"></ReactHighcharts>
												</Row>
											</div>;
										}
									})()}
								</TabPane>
								<TabPane tab="Device-3" key="device-3">
									{(() => {
										if (this.state.gridVisible) {
											return <div>
												<Table columns={columns} dataSource={data} scroll={{ y: 540 }} />
											</div>
										} else {
											return <div>
												<Row>
													<ReactHighcharts config={graph1} ref="graph"></ReactHighcharts>
												</Row>
											</div>;
										}
									})()}
									{/*<Tabs type="card">
										<TabPane tab="Grid" key="grid">
											<Table columns={columns} dataSource={data} />
										</TabPane>
										<TabPane tab="Graph" key="graph">
											<Row>
												<ReactHighcharts config={graph1} ref="graph"></ReactHighcharts>
											</Row>
										</TabPane>
									</Tabs>*/}
								</TabPane>
							</Tabs>
						</Form>
						{/*<div className="draw-btn-container">
							<Button
								style={{
									marginRight: 8,
								}}
								onClick={onCancel}
							>
								Cancel
							</Button>
							<Button onClick={onCancel} type="primary">Submit</Button>
						</div>*/}
					</Drawer>
				</div>
			);
		}
	}
);

const deviceData = [{
	title: 'Device-1',
	value: 'device-1',
	key: '0-0',
}, {
	title: 'Device-2',
	value: 'device-2',
	key: '0-1',
}, {
	title: 'Device-3',
	value: 'device-3',
	key: '0-2',
}, {
	title: 'Device-4',
	value: 'device-4',
	key: '0-3',
}, {
	title: 'Device-5',
	value: 'device-5',
	key: '0-4',
}];

class Archive extends React.Component {

	state = {
		value1: 1,
		value2: 1,
		value3: 1,
		value4: 1,
		startValue: null,
		endValue: null,
		valueDevice: [],
		endOpen: false,
		drawDataVisible: false,
	};

	disabledStartDate = (startValue) => {
		const endValue = this.state.endValue;
		if (!startValue || !endValue) {
			return false;
		}
		return startValue.valueOf() > endValue.valueOf();
	}

	onChangeDevice = (valueDevice) => {
		this.setState({ valueDevice });
	}

	showDataDrawer = () => {
		this.setState({ drawDataVisible: true });
	}

	handleCancel = () => {
		this.setState({ drawDataVisible: false });
	}

	disabledEndDate = (endValue) => {
		const startValue = this.state.startValue;
		if (!endValue || !startValue) {
			return false;
		}
		return endValue.valueOf() <= startValue.valueOf();
	}

	onChange = (field, value) => {
		this.setState({
			[field]: value,
		});
	}

	onStartChange = (value) => {
		this.onChange('startValue', value);
	}

	onEndChange = (value) => {
		this.onChange('endValue', value);
	}

	handleStartOpenChange = (open) => {
		if (!open) {
			this.setState({ endOpen: true });
		}
	}

	handleEndOpenChange = (open) => {
		this.setState({ endOpen: open });
	}

	onChangeAqi = (checkedList) => {
		this.setState({
			checkedList,
			indeterminate: !!checkedList.length && (checkedList.length < aqi.length),
		});
	}

	onCheckAllChangeAqi = (e) => {
		this.setState({
			checkedList: e.target.checked ? aqi : [],
			indeterminate: false,
			checkAll: e.target.checked,
		});
	}

	onChangeWeather = (checkedList) => {
		this.setState({
			checkedList,
			indeterminate: !!checkedList.length && (checkedList.length < weather.length),
		});
	}

	onCheckAllChangeWeather = (e) => {
		this.setState({
			checkedList: e.target.checked ? weather : [],
			indeterminate: false,
			checkAll: e.target.checked,
		});
	}

	onChangeOther = (checkedList) => {
		this.setState({
			checkedList,
			indeterminate: !!checkedList.length && (checkedList.length < other.length),
		});
	}

	onCheckAllChangeOther = (e) => {
		this.setState({
			checkedList: e.target.checked ? other : [],
			indeterminate: false,
			checkAll: e.target.checked,
		});
	}

	onChange1 = (e) => {
		console.log('radio checked', e.target.value);
		this.setState({
			value1: e.target.value,
		});
	}
	onChange2 = (e) => {
		console.log('radio checked', e.target.value);
		this.setState({
			value2: e.target.value,
		});
	}
	onChange3 = (e) => {
		console.log('radio checked', e.target.value);
		this.setState({
			value3: e.target.value,
		});
	}
	onChange4 = (e) => {
		console.log('radio checked', e.target.value);
		this.setState({
			value4: e.target.value,
		});
	}

	render () {
		const { startValue, endValue, endOpen } = this.state;
		const deviceProps = {
			treeData: deviceData,
			value: this.state.valueDevice,
			onChange: this.onChangeDevice,
			treeCheckable: true,
			showCheckedStrategy: SHOW_PARENT,
			searchPlaceholder: 'Please select Device',
		};
		return (
			<div id="archive" className="mar-top-70">
				<Side active_link="archive" />
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="head">Archive Your Data</div>
						<Form layout="vertical" hideRequiredMark>
							<Row gutter={50}>
								<Col span={12} className="wid-100">
									<Form.Item label="Select City">
										<Select showSearch placeholder="Please select city">
											{city}
										</Select>
									</Form.Item>
								</Col>
								<Col span={12} className="wid-100">
									<Form.Item label="Select Device">
										<TreeSelect showSearch treeDefaultExpandAll {...deviceProps} />
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={50}>
								<Col span={12} className="wid-100">
									<Form.Item label="Data Type">
										<RadioGroup onChange={this.onChange1} value={this.state.value1}>
											<Radio value={1}>Summary</Radio>
											<Radio value={2}>Raw</Radio>
											<Radio value={3}>Average</Radio>
										</RadioGroup>
									</Form.Item>
								</Col>
								<Col span={12} className="wid-100 date-pick">
									<Form.Item label="Time Interval">
										<DatePicker
											disabledDate={this.disabledStartDate}
											showTime
											format="YYYY-MM-DD HH:mm:ss"
											value={startValue}
											placeholder="From Date "
											onChange={this.onStartChange}
											onOpenChange={this.handleStartOpenChange}
										/>
										<span className="separator"> - </span>
										<DatePicker
											disabledDate={this.disabledEndDate}
											showTime
											format="YYYY-MM-DD HH:mm:ss"
											value={endValue}
											placeholder="To Date"
											onChange={this.onEndChange}
											open={endOpen}
											onOpenChange={this.handleEndOpenChange}
										/>
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={50}>
								<Col span={12} className="wid-100">
									<Form.Item label="Data Type">
										<RadioGroup onChange={this.onChange2} value={this.state.value2}>
											<Radio value={1}>Grid</Radio>
											<Radio value={2}>Raw</Radio>
										</RadioGroup>
									</Form.Item>
								</Col>
								<Col span={12} className="wid-100">
									<Form.Item label="Conversion Type">
										<RadioGroup onChange={this.onChange3} value={this.state.value3}>
											<Radio value={1}>USEPA</Radio>
											<Radio value={2}>NAQI</Radio>
										</RadioGroup>
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={50}>
								<Col span={12} className="wid-100">
									<Form.Item label="Download Format">
										<RadioGroup onChange={this.onChange4} value={this.state.value4}>
											<Radio value={1}>Excel</Radio>
											<Radio value={2}>Pdf</Radio>
										</RadioGroup>
									</Form.Item>
								</Col>
							</Row>
						</Form>
					</Content>
					<Content className="contain butn-contain">
						<Button className="button" type="primary" icon="eye-o" onClick={this.showDataDrawer}>View</Button>
						<Button className="button" type="primary" icon="download" >Download</Button>
					</Content>
				</Layout>
				<ShowData 
					visible={this.state.drawDataVisible}
					onCancel={this.handleCancel}
					onCreate={this.handleCancel}
				/>
			</div>
		);
	}
}

export default Archive;