import React from 'react';
import { Layout, Row, Col, Button, Icon, Skeleton, Spin } from 'antd';
import './notification.less';
import Head from './Head.jsx';
import Side from './Side.jsx';

const { Content } = Layout;

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} />;

class Notification_loader extends React.Component {

	render () {
		return (
			<div id="notification" className="mar-top-70 notification-loader">
				<Side/>
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="head"><Skeleton className="hed-load" active /></div>
						<div className="table-filter"><Skeleton className="select-load" active /></div>
						<div className="table-search">
							<Skeleton className="select-load" active />
						</div>
						<Row className="min-height">
							<Spin indicator={antIcon} />
						</Row>
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Notification_loader;