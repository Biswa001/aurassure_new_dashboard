import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Modal, Drawer, Card } from 'antd';
import './compare.less';
import Head from './Head.jsx';
import Side from './Side.jsx';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];
const confirm = Modal.confirm;

const config = {
	chart: {
		type: 'column',
		height: 80,
		width: 200,
		backgroundColor: 'transparent',
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}				
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: '',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
	},
	yAxis: {
		title: {
			text: ''
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	xAxis: {
		title: {
			text: 'Past 24 hr AQI Trend'
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	tooltip: {
		pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
	},
					
	series: [{
		data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

const config2 = {
	chart: {
		type: 'column',
		height: 55,
		width: 180,
		backgroundColor: 'transparent',
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}				
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: '',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
	},
	yAxis: {
		title: {
			text: ''
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	xAxis: {
		title: {
			text: ' '
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	tooltip: {
		pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
	},
					
	series: [{
		data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

function showDelete() {
	confirm({
		title: 'Do you want to delete ?',
		content: '',
		onOk() {},
		onCancel() {},
	});
}

const action = (
	<Menu>
		<Menu.Item key="action-1">Edit</Menu.Item>
		<Menu.Item key="action-2" onClick={showDelete}>Delete</Menu.Item>
	</Menu>
);

const city = [];
for (let i = 1; i < 12; i++) {
	city.push(<Option key={'City - ' + i}>{'City - ' + i}</Option>);
}

const station = [];
for (let i = 1; i < 12; i++) {
	station.push(<Option key={'Sation - ' + i}>{'Station - ' + i}</Option>);
}

class Compare extends React.Component {

	state = {
		visibleModal: false
	};

	showModal = () => {
		this.setState({
			visibleModal: true,
		});
	}

	handleCancel = (e) => {
		this.setState({
			visibleModal: false,
		});
	}

	render () {

		return (
			<div id="compare" className="mar-top-70">
				<Side active_link="compare" />
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="header-txt">
							City Comparision
						</div>
						<div className="btn-contain">
							<Button className="add-btn" type="primary" icon="plus" onClick={this.showModal}>Add New</Button>
						</div>
						<div className="card-container">
							<div className="card-contain">
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="City-1">
									<Tooltip title="Last data received Time">
										<div className="city-time">
											{/*<span className="dot"></span>*/}
											16:49, 03 Sep 2018
										</div>
									</Tooltip>
									<Row>
										<span className="aqi">AQI</span>
										<span className="aqi-value-txt">60</span>
										<span className="success">Satisfactory</span>
									</Row>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="City-2">
									<Tooltip title="Last data received Time">
										<div className="city-time">
											{/*<span className="dot"></span>*/}
											16:49, 03 Sep 2018
										</div>
									</Tooltip>
									<Row>
										<span className="aqi">AQI</span>
										<span className="aqi-value-txt">150</span>
										<span className="danger">Polluted</span>
									</Row>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="City-3">
									<Tooltip title="Last data received Time">
										<div className="city-time">
											{/*<span className="dot"></span>*/}
											16:49, 03 Sep 2018
										</div>
									</Tooltip>
									<Row>
										<span className="aqi">AQI</span>
										<span className="aqi-value-txt">100</span>
										<span className="warning">Satisfactory</span>
									</Row>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="City-4">
									<Tooltip title="Last data received Time">
										<div className="city-time">
											{/*<span className="dot"></span>*/}
											16:49, 03 Sep 2018
										</div>
									</Tooltip>
									<Row>
										<span className="aqi">AQI</span>
										<span className="aqi-value-txt">60</span>
										<span className="success">Satisfactory</span>
									</Row>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="City-5">
									<Tooltip title="Last data received Time">
										<div className="city-time">
											{/*<span className="dot"></span>*/}
											16:49, 03 Sep 2018
										</div>
									</Tooltip>
									<Row>
										<span className="aqi">AQI</span>
										<span className="aqi-value-txt">60</span>
										<span className="success">Satisfactory</span>
									</Row>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="City-6">
									<Tooltip title="Last data received Time">
										<div className="city-time">
											{/*<span className="dot"></span>*/}
											16:49, 03 Sep 2018
										</div>
									</Tooltip>
									<Row>
										<span className="aqi">AQI</span>
										<span className="aqi-value-txt">60</span>
										<span className="success">Satisfactory</span>
									</Row>
								</Card>
							</div>
						</div>

						<Modal
									title="New Section"
									visible={this.state.visibleModal}
									onOk={this.handleCancel}
									onCancel={this.handleCancel}
								>
										<Form layout="vertical" hideRequiredMark>
								<Row>
									<Col span={24} className="wid-100">
										<Form.Item label="Select City">
											<Select showSearch placeholder="Please select city">
												{city}
											</Select>
										</Form.Item>
									</Col>
								</Row>
							</Form>
						</Modal>

					</Content>

					<Content className="contains mar-top-50 compare-container">
						<div className="header-txt">
							Station Comparision
						</div>
						<div className="select-menu">
							<Select className="select-icon" showSearch placeholder="Please select city">
								{city}
							</Select>
						</div>
						<div className="select-menu pad-left">
							<Select className="select-icon" showSearch placeholder="Please select station">
								{station}
							</Select>
						</div>
						<div className="card-container">
							<div className="compare-contain">
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="Station-1">
									<div className="time">
										<Tooltip title="Last data received Time">
											<span className="dot"></span>
											16:49, 03 Sep 2018
										</Tooltip>
									</div>
									<Row gutter="6" type="flex" justify="space-around" align="middle">
										<Col span="4">
											<img src="http://127.0.0.1:8080/aurassure_logo.svg"/>
										</Col>
										<Col span="4" className="center">
											<div className="value-txt">60</div>
											<div>AQI</div>
										</Col>
										<Col span="12">
											<div className="success">Satisfactory</div>
											<div className="polutant">Pollutant - <span className="polutant-name">SO<sub>2</sub></span></div>
										</Col>
									</Row>
									<Row className="hr24-graph">
										<Col span={17}>
											<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
										</Col>
										<Col span={6}>
											<span className="value">
												<div className="graph-value">67</div>
												<div>Min</div>
											</span>
											<span className="value">
												<div className="graph-value">99</div>
												<div>Max</div>
											</span>
										</Col>
									</Row>
									<div className="aqi-scale"><img src="http://127.0.0.1:8080/aqi_scale.svg" /></div>
									<div className="head">Last 24 Hrs Data</div>
									<div className="24hr-data graphs">
										<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">PM <sub>2.5</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
												</div>
												<Col span={17}>
													<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">CO <sub></sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
												</div>
												<Col span={17}>
													<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">O <sub>3</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value success">50</span>AQI</span>
												</div>
												<Col span={17}>
													<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">SO <sub>2</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
												</div>
												<Col span={17}>
													<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">CO <sub>2</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
												</div>
												<Col span={17}>
													<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
											<Row className="hr24-graph" align="bottom">
												<div className="graph-name">
													<span className="pollutant-name">PM <sub>10</sub></span>
													<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
												</div>
												<Col span={17}>
													<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
												</Col>
												<Col span={5}>
													<span className="value">
														<div className="graph-value">67</div>
														<div>Min</div>
													</span>
													<span className="value">
														<div className="graph-value">99</div>
														<div>Max</div>
													</span>
												</Col>
											</Row>
									</div>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="Station-2">
									<div className="time">
										<Tooltip title="Last data received Time">
											<span className="dot"></span>
											16:49, 03 Sep 2018
										</Tooltip>
									</div>
									<Row gutter="6" type="flex" justify="space-around" align="middle">
										<Col span="4">
											<img src="http://127.0.0.1:8080/aurassure_logo.svg"/>
										</Col>
										<Col span="4" className="center">
											<div className="value-txt">60</div>
											<div>AQI</div>
										</Col>
										<Col span="12">
											<div className="success">Satisfactory</div>
											<div className="polutant">Pollutant - <span className="polutant-name">SO<sub>2</sub></span></div>
										</Col>
									</Row>
									<Row className="hr24-graph">
										<Col span={17}>
											<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
										</Col>
										<Col span={6}>
											<span className="value">
												<div className="graph-value">67</div>
												<div>Min</div>
											</span>
											<span className="value">
												<div className="graph-value">99</div>
												<div>Max</div>
											</span>
										</Col>
									</Row>
									<div className="aqi-scale"><img src="http://127.0.0.1:8080/aqi_scale.svg" /></div>
									<div className="head">Last 24 Hrs Data</div>
									<div className="24hr-data graphs">
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">PM <sub>2.5</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">CO <sub></sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">O <sub>3</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value success">50</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">SO <sub>2</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">CO <sub>2</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">PM <sub>10</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
									</div>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="Station-3">
									<div className="time">
										<Tooltip title="Last data received Time">
											<span className="dot"></span>
											16:49, 03 Sep 2018
										</Tooltip>
									</div>
									<Row gutter="6" type="flex" justify="space-around" align="middle">
										<Col span="4">
											<img src="http://127.0.0.1:8080/aurassure_logo.svg"/>
										</Col>
										<Col span="4" className="center">
											<div className="value-txt">60</div>
											<div>AQI</div>
										</Col>
										<Col span="12">
											<div className="success">Satisfactory</div>
											<div className="polutant">Pollutant - <span className="polutant-name">SO<sub>2</sub></span></div>
										</Col>
									</Row>
									<Row className="hr24-graph">
										<div className="danger center">No data received in last 24 hours.</div>
									</Row>
									<div className="head">Last 24 Hrs Data</div>
									<div className="24hr-data graphs">
										<div className="danger center no-data-txt">No data received in last 24 hours.</div>
									</div>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="Station-4">
									<Tooltip title="Last data received Time">
										<div className="time">
											<span className="dot"></span>
											16:49, 03 Sep 2018
										</div>
									</Tooltip>
									<Row gutter="6" type="flex" justify="space-around" align="middle">
										<Col span="4">
											<img src="http://127.0.0.1:8080/aurassure_logo.svg"/>
										</Col>
										<Col span="4" className="center">
											<div className="value-txt">60</div>
											<div>AQI</div>
										</Col>
										<Col span="12">
											<div className="success">Satisfactory</div>
											<div className="polutant">Pollutant - <span className="polutant-name">SO<sub>2</sub></span></div>
										</Col>
									</Row>
									<Row className="hr24-graph">
										<Col span={17}>
											<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
										</Col>
										<Col span={6}>
											<span className="value">
												<div className="graph-value">67</div>
												<div>Min</div>
											</span>
											<span className="value">
												<div className="graph-value">99</div>
												<div>Max</div>
											</span>
										</Col>
									</Row>
									<div className="aqi-scale"><img src="http://127.0.0.1:8080/aqi_scale.svg" /></div>
									<div className="head">Last 24 Hrs Data</div>
									<div className="24hr-data graphs">
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">PM <sub>2.5</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">CO <sub></sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">O <sub>3</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value success">50</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">SO <sub>2</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">CO <sub>2</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">PM <sub>10</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
									</div>
								</Card>
								<Card extra={<Icon type="close" className="close-btn" theme="outlined" />} className="card" title="Station-5">
									<div className="time">
										<Tooltip title="Last data received Time">
											<span className="dot"></span>
											16:49, 03 Sep 2018
										</Tooltip>
									</div>
									<Row gutter="6" type="flex" justify="space-around" align="middle">
										<Col span="4">
											<img src="http://127.0.0.1:8080/aurassure_logo.svg"/>
										</Col>
										<Col span="4" className="center">
											<div className="value-txt">60</div>
											<div>AQI</div>
										</Col>
										<Col span="12">
											<div className="success">Satisfactory</div>
											<div className="polutant">Pollutant - <span className="polutant-name">SO<sub>2</sub></span></div>
										</Col>
									</Row>
									<Row className="hr24-graph">
										<Col span={17}>
											<ReactHighcharts config={config} ref="chart"></ReactHighcharts>
										</Col>
										<Col span={6}>
											<span className="value">
												<div className="graph-value">67</div>
												<div>Min</div>
											</span>
											<span className="value">
												<div className="graph-value">99</div>
												<div>Max</div>
											</span>
										</Col>
									</Row>
									<div className="aqi-scale"><img src="http://127.0.0.1:8080/aqi_scale.svg" /></div>
									<div className="head">Last 24 Hrs Data</div>
									<div className="24hr-data graphs">
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">PM <sub>2.5</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">CO <sub></sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">O <sub>3</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value success">50</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">SO <sub>2</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">CO <sub>2</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
										<Row className="hr24-graph" align="bottom">
											<div className="graph-name">
												<span className="pollutant-name">PM <sub>10</sub></span>
												<span className="graph-aqi"><span className="graph-aqi-value danger">120</span>AQI</span>
											</div>
											<Col span={17}>
												<ReactHighcharts config={config2} ref="chart"></ReactHighcharts>
											</Col>
											<Col span={5}>
												<span className="value">
													<div className="graph-value">67</div>
													<div>Min</div>
												</span>
												<span className="value">
													<div className="graph-value">99</div>
													<div>Max</div>
												</span>
											</Col>
										</Row>
									</div>
								</Card>
							</div>
						</div>
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Compare;