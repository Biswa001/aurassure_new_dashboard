import React from 'react';
import { Layout, Row, Col, Button, Select, Icon, Skeleton, Spin, Tabs, Checkbox, Input, TreeSelect, Table, Tooltip, Form, Menu, Dropdown, Modal, Drawer, Card } from 'antd';
import './compare.less';
import Head from './Head.jsx';
import Side from './Side.jsx';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} />;

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;
const { Option } = Select;
const CheckboxGroup = Checkbox.Group;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];
const confirm = Modal.confirm;

const config = {
	chart: {
		type: 'column',
		height: 80,
		width: 200
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}				
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: '',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
	},
	yAxis: {
		title: {
			text: ''
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	xAxis: {
		title: {
			text: 'Past 24 hr AQI Trend'
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	tooltip: {
		pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
	},
					
	series: [{
		data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

const config2 = {
	chart: {
		type: 'column',
		height: 55,
		width: 180
	},
	plotOptions: {
		series: {
			pointPadding: 0,
			groupPadding: 0
		}				
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			enabled: true,
			text: '',
			style: {
				fontWeight: 'normal'
			}
		},
		type: '',
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
	},
	yAxis: {
		title: {
			text: ''
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	xAxis: {
		title: {
			text: ' '
		},
		lineWidth: 0,
		minorGridLineWidth: 0,
		lineColor: 'transparent',
		labels: {
			enabled: false
		},
		minorTickLength: 0,
		tickLength: 0,
		gridLineColor: 'transparent'

	},
	legend: {
		enabled: false
	},
	tooltip: {
		pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
	},
					
	series: [{
		data: [67.0, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.8, 95.6, 114.4, 219.4, 67.9, 120.8, 92.78, 87.9, 83.8, 111.1, 78.9, 92.0, 220.1, 56.9, 44.4]
	}]
};

function showDelete() {
	confirm({
		title: 'Do you want to delete ?',
		content: '',
		onOk() {},
		onCancel() {},
	});
}

const action = (
	<Menu>
		<Menu.Item key="action-1">Edit</Menu.Item>
		<Menu.Item key="action-2" onClick={showDelete}>Delete</Menu.Item>
	</Menu>
);

const city = [];
for (let i = 1; i < 12; i++) {
	city.push(<Option key={'City - ' + i}>{'City - ' + i}</Option>);
}

const station = [];
for (let i = 1; i < 12; i++) {
	station.push(<Option key={'Sation - ' + i}>{'Station - ' + i}</Option>);
}

class Compare_loader extends React.Component {

	state = {
		visibleModal: false
	};

	showModal = () => {
		this.setState({
			visibleModal: true,
		});
	}

	handleCancel = (e) => {
		this.setState({
			visibleModal: false,
		});
	}

	render () {

		return (
			<div id="compare" className="mar-top-70 compare-loader">
				<Side active_link="compare" />
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="header-txt">
							<Skeleton className="hed-load" active />
						</div>
						<div className="btn-contain">
							
						</div>
						<div className="card-container">
							<div className="card-contain">
								<Card  className="card" title="">
									<Skeleton className="avtar-load" active avatar />
								</Card>
								<Card className="card" title="">
									<Skeleton className="avtar-load" active avatar />
								</Card>
								<Card className="card" title="">
									<Skeleton className="avtar-load" active avatar />
								</Card>
								<Card className="card" title="">
									<Skeleton className="avtar-load" active avatar />
								</Card>
							</div>	
						</div>
					</Content>

					<Content className="contains mar-top-50 compare-container">
						<div className="header-txt">
							<Skeleton className="hed-load" active />
						</div>
						<div className="select-menu">
							<Skeleton className="select-load" active />
						</div>
						<div className="select-menu pad-left">
							<Skeleton className="select-load" active />
						</div>
						<div className="card-container">
							<div className="compare-contain">
								<Card className="card" title="">
									<Skeleton className="avtar-load" active avatar />
								</Card>
								<Card className="card" title="">
									<Skeleton className="avtar-load" active avatar />
								</Card>
								<Card className="card" title="">
									<Skeleton className="avtar-load" active avatar />
								</Card>
							</div>
						</div>
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Compare_loader;