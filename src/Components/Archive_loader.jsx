import React from 'react';
import { Layout, Row, Col, Button, Select, Skeleton, Spin, Icon, Tabs, Drawer, Radio, DatePicker, Checkbox, Form, Input, Table } from 'antd';
import './archive.less';
import Head from './Head.jsx';
import Side from './Side.jsx';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} />;

const TabPane = Tabs.TabPane;
const { Content } = Layout;
const { Option } = Select;
const aqi = ['aqi1', 'aqi2', 'aqi3'];
const weather = ['weather1', 'weather2', 'weather3'];
const other = ['other1', 'other2', 'other3'];

const graph1 = {
	chart: {
				type: 'line',
				height: 400
		},
		title: {
				text: ''
		},
		xAxis: {
				categories: ['DAY-1', 'DAY-2', 'DAY-3', 'DAY-4', 'DAY-5']
		},
		yAxis: {
				title: {
						text: ''
				},
				labels: {
						enabled: false
				}
		},
		plotOptions: {
				line: {
						dataLabels: {
								enabled: true
						},
						enableMouseTracking: true
				}
		},
		series: [{
			showInLegend: false, 
				name: '',
				data: [7.0, 6.9, 9.5, 7.5, 1.4]
		}]
};

const graph2 = {
	chart: {
				type: 'line',
				height: 400
		},
		title: {
				text: ''
		},
		xAxis: {
				categories: ['DAY-1', 'DAY-2', 'DAY-3', 'DAY-4', 'DAY-5']
		},
		yAxis: {
				title: {
						text: ''
				},
				labels: {
						enabled: false
				}
		},
		plotOptions: {
				line: {
						dataLabels: {
								enabled: true
						},
						enableMouseTracking: true
				}
		},
		series: [{
				name: 'Max',
				data: [7.0, 6.9, 9.5, 11.5, 7.4]
		}, {
				name: 'Min',
				data: [3.9, 4.2, 5.7, 8.5, 4.9]
		}]
};

const data = [
	{
		parameter: 'Temperature(°C)',
		avg: 29.24,
		min: 25.40,
		minat: '03:37, 27 Sep',
		max: 36.20,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'Humidity(%)',
		avg: 92.17,
		min: 62.40,
		minat: '03:37, 27 Sep',
		max: 99.20,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'SO2(ppb)',
		avg: 33.51,
		min: 6.82,
		minat: '03:37, 27 Sep',
		max: 75.31,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'NO2(ppb)',
		avg: 11.03,
		min: 2.37,
		minat: '03:37, 27 Sep',
		max: 19.02,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'NO(ppb)',
		avg: 25.99,
		min: 4.30,
		minat: '03:37, 27 Sep',
		max: 48.65,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'CO(ppm)',
		avg: 0.3937,
		min: 0.2840,
		minat: '03:37, 27 Sep',
		max: 0.6961,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'O2(% vol.)',
		avg: 20.90,
		min: 20.23,
		minat: '03:37, 27 Sep',
		max: 26.14,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'Noise(dB (A))',
		avg: 33.08,
		min: 28.17,
		minat: '03:37, 27 Sep',
		max: 74.11,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'PM1(μg / m3)',
		avg: 49.44,
		min: 32.00,
		minat: '03:37, 27 Sep',
		max: 209.00,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'PM2.5(μg / m3)',
		avg: 67.81,
		min: 46.00,
		minat: '03:37, 27 Sep',
		max: 258.00,
		maxat: '11:37, 27 Sep'
	}, {
		parameter: 'PM10(μg / m3)',
		avg: 83.06,
		min: 58.00,
		minat: '03:37, 27 Sep',
		max: 292.00,
		maxat: '11:37, 27 Sep'
	}];

const ShowData = Form.create()(
	class extends React.Component {

		constructor() {
			super()
			this.handleCheckBox = this.handleCheckBox.bind(this)
			this.state = {
				gridVisible: true
			}
		}

		handleCheckBox(e) {
			this.setState({
				gridVisible: !this.state.gridVisible
			})
		}

		render() {
			const { visible, onCancel, onCreate, form } = this.props;
			const { getFieldDecorator } = form;

			const columns = [{
				title: 'Parameter',
				width: 120,
				key: 'parameter',
				dataIndex: 'parameter'
			}, {
				title: 'Avg',
				width: 80,
				dataIndex: 'avg',
				key: 'avg'
			}, {
				title: 'Min',
				width: 80,
				dataIndex: 'min',
				key: 'min'
			}, {
				title: 'Min at.',
				width: 100,
				dataIndex: 'minat',
				key: 'minat'
			}, {
				title: 'Max',
				width: 80,
				dataIndex: 'max',
				key: 'max'
			}, {
				title: 'Max at.',
				width: 100,
				dataIndex: 'maxat',
				key: 'maxat'
			}];
			
			return (
				<div id="group_form">
					<Drawer
						title="Summary Report"
						width={1000}
						placement="right"
						visible={visible}
						onClose={onCancel}
						maskClosable={true}
						style={{
							height: 'calc(100% - 55px)',
							overflow: 'auto',
							paddingBottom: 53,
						}}
					>
						<Form layout="vertical" hideRequiredMark>
							<Tabs type="card">
								<TabPane tab="Device-1" key="device-1">
									<div className="view-online">
										<span>Grid</span>
										<div className='wrapper'>
											<input type='checkbox' id='checkable1' onChange={this.handleCheckBox} checked={this.state.checked} />
											<label for='checkable1'></label>
										</div>
										<span>Graph</span>
									</div>
									{(() => {
										if (this.state.gridVisible) {
											return <div>
												<Table columns={columns} dataSource={data} />
											</div>
										}
										else {
											<div>
												<Row>
													<ReactHighcharts config={graph1} ref="graph"></ReactHighcharts>
												</Row>
												<Row>
													<ReactHighcharts config={graph2} ref="graph"></ReactHighcharts>
												</Row>
											</div>
										}
									})()}
								</TabPane>
								<TabPane tab="Device-2" key="device-2">
									<Tabs type="card">
										<TabPane tab="Grid" key="grid">
											<Table columns={columns} dataSource={data} />
										</TabPane>
										<TabPane tab="Graph" key="graph">
											<Row>
												<ReactHighcharts config={graph2} ref="graph"></ReactHighcharts>
											</Row>
										</TabPane>
									</Tabs>
								</TabPane>
								<TabPane tab="Device-3" key="device-3">
									<Tabs type="card">
										<TabPane tab="Grid" key="grid">
											<Table columns={columns} dataSource={data} />
										</TabPane>
										<TabPane tab="Graph" key="graph">
											<Row>
												<ReactHighcharts config={graph1} ref="graph"></ReactHighcharts>
											</Row>
										</TabPane>
									</Tabs>
								</TabPane>
							</Tabs>
						</Form>
					</Drawer>
				</div>
			);
		}
	}
);



class Archive_loader extends React.Component {

	state = {
		drawDataVisible: false,
	};

	render () {
		return (
			<div id="archive" className="mar-top-70 archive-loader">
				<Side active_link="archive" />
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="head"><Skeleton className="hed-load" active /></div>
						<Form layout="vertical" hideRequiredMark>
							<Row gutter={50}>
								<Col span={12} className="wid-100">
									<Form.Item label="">
										<Skeleton className="list-load" active />
									</Form.Item>
								</Col>
								<Col span={12} className="wid-100">
									<Form.Item label="">
										<Skeleton className="list-load" active />
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={50}>
								<Col span={12} className="wid-100">
									<Form.Item label="">
										<Skeleton className="list-load" active />
									</Form.Item>
								</Col>
								<Col span={12} className="wid-100">
									<Form.Item label="">
										<Skeleton className="list-load" active />
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={50}>
								<Col span={12} className="wid-100">
									<Form.Item label="">
										<Skeleton className="list-load" active />
									</Form.Item>
								</Col>
								<Col span={12} className="wid-100">
									<Form.Item label="">
										<Skeleton className="list-load" active />
									</Form.Item>
								</Col>
							</Row>
							<Row gutter={50}>
								<Col span={12} className="wid-100">
									<Form.Item label="">
										<Skeleton className="list-load" active />
									</Form.Item>
								</Col>
							</Row>
						</Form>
					</Content>
					{/*<Content className="contain">
						<Button className="button" type="primary" size="large" onClick={this.showDataDrawer}>View</Button>
						<Button className="button" type="primary" icon="download" size="large">Download</Button>
					</Content>*/}
				</Layout>
				<ShowData 
					visible={this.state.drawDataVisible}
					onCancel={this.handleCancel}
					onCreate={this.handleCancel}
				/>
			</div>
		);
	}
}

export default Archive_loader;