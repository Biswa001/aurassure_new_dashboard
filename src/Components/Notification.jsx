import React from 'react';
import { Layout, Row, Col, Button, Icon, Input, TreeSelect, Table, Tooltip } from 'antd';
import './notification.less';
import Head from './Head.jsx';
import Side from './Side.jsx';

const SHOW_PARENT = TreeSelect.SHOW_PARENT;
const { Content } = Layout;

const data = [{
	sl: 1,
	date: '13:34, 28 Sep',
	city: 'City-1',
	device: 'Device-1',
	msg: 'SO2 of CEMS - 1 above warning limit.'
}, {
	sl: 2,
	date: '13:34, 28 Sep',
	city: 'City-2',
	device: 'Device-2',
	msg: 'SO2 of CEMS - 1 above warning limit.'
}, {
	sl: 3,
	date: '23:34, 28 Sep',
	city: 'City-3',
	device: 'Device-3',
	msg: 'SO2 of CEMS - 1 above warning limit.'
}, {
	sl: 4,
	date: '13:34, 28 Sep',
	city: 'City-4',
	device: 'Device-4',
	msg: 'SO2 of CEMS - 1 above warning limit.'
}, {
	sl: 5,
	date: '12:34, 28 Sep',
	city: 'City-5',
	device: 'Device-5',
	msg: 'SO2 of CEMS - 1 above warning limit.'
}, {
	sl: 6,
	date: '13:34, 28 Sep',
	city: 'City-6',
	device: 'Device-6',
	msg: 'SO2 of CEMS - 1 above warning limit.'
}];

const userFilter = [{
	title: 'Status',
	value: 'status',
	key: '0-0',
	children: [{
		title: 'Online',
		value: 'online',
		key: '0-0-0',
	}, {
		title: 'Offline',
		value: 'offline',
		key: '0-0-1',
	}],
}, {
	title: 'City',
	value: 'city',
	key: '0-1',
	children: [{
		title: 'City-1',
		value: 'city-1',
		key: '0-1-0',
	}, {
		title: 'City-2',
		value: 'city-2',
		key: '0-1-1',
	}, {
		title: 'City-3',
		value: 'city-3',
		key: '0-1-2',
	}, {
		title: 'City-4',
		value: 'city-4',
		key: '0-1-2',
	}, {
		title: 'City-5',
		value: 'city-5',
		key: '0-1-2',
	}, {
		title: 'City-6',
		value: 'city-6',
		key: '0-1-2',
	}, {
		title: 'City-7',
		value: 'city-7',
		key: '0-1-2',
	}, {
		title: 'City-8',
		value: 'city-8',
		key: '0-1-2',
	}],
}];


class Notification extends React.Component {

	state = {
		value: [],
	};

	render () {
		const { startValue, endValue, endOpen } = this.state;
		const userProps = {
			treeData: userFilter,
			value: this.state.value,
			onChange: this.onChangeFilter,
			treeCheckable: true,
			showCheckedStrategy: SHOW_PARENT,
			searchPlaceholder: 'Please select filter',
		};

		const columns = [{
			title: 'Sl No',
			width: 50,
			key: 'sl',
			dataIndex: 'sl',
			align: 'center'
		}, {
			title: 'Date',
			width: 100,
			dataIndex: 'date',
			key: 'date',
			sorter: (a, b) => a.date.length - b.date.length,
		}, {
			title: 'City',
			width: 100,
			dataIndex: 'city',
			key: 'city',
			sorter: (a, b) => a.city.length - b.city.length,
		}, {
			title: 'Device',
			width: 100,
			dataIndex: 'device',
			key: 'device',
			sorter: (a, b) => a.device.length - b.device.length,
		}, {
			title: 'Message',
			width: 250,
			dataIndex: 'msg',
			key: 'msg',
			sorter: (a, b) => a.msg.length - b.msg.length,
		}];

		return (
			<div id="notification" className="mar-top-70">
				<Side/>
				<Head/>
				<Layout className="mar-top-72">
					<Content className="contains">
						<div className="head">Notifications</div>
						<div className="table-filter"><TreeSelect treeDefaultExpandAll {...userProps} className="filter-icon" /></div>
						<div className="table-search">
							<Input placeholder="Search" prefix={<Icon type="search" />} />
						</div>
						<Row className="table-container">
							<Table columns={columns} dataSource={data} />
						</Row>
					</Content>
				</Layout>
			</div>
		);
	}
}

export default Notification;